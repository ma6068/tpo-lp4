var express = require('express');
var router = express.Router();

var ctrlOther = require('../controllers/other');

router.get('/', ctrlOther.angularApp);

module.exports = router;