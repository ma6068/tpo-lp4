var mongoose = require('mongoose');
require("../models/token");
var Token = mongoose.model('token');
var User = mongoose.model('user');
require('../models/token');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
};

module.exports.confirmToken = function (req,res) {
    Token.findOne({ token: req.params.token }, function (err, token) {
        if(err){
            JSONResponse(res, 400, err);
            return;
        }
        if (!token){ 
            JSONResponse(res, 400, {
               message: 'We were unable to find a validate token. Your token may have expired.' 
            });
            return;
        }
        // If we found a token, find a matching user
        User.findOne({ _id: token._userId }, function (err, user) {
            if(err){
                JSONResponse(res, 400, err);
                return;
            }
            if (!user){ 
                JSONResponse(res, 400, {
                   message: 'We were unable to find a user for this token.' 
                });
                return;
            }
            if (user.confirmationStatus == 1) {
                JSONResponse(res, 400, {
                   message: 'This user has already been verified..' 
                });
                return;
            }
            // Verify and save the user
            user.confirmationStatus = 1;
            user.save(function (err) {
                if (err) { 
                    JSONResponse(res, 400, err);
                    return; 
                }else{
                    JSONResponse(res, 200, {
                        message: 'Account verified.'
                    });
                }
            });
        });
    });
};