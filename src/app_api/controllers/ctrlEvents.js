var mongoose = require('mongoose');
require('../models/events');
var events = mongoose.model('events');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
};

module.exports.eventsGetAll = function(req, res){
    events.find({})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, {"status" : "There was an errror"});
        }else{
            JSONResponse(res, 200, content);
        }
    });
};

module.exports.eventsAdd = function(req, res){
    if(!req.body.name || !req.body.date || !req.body.org || !req.body.desc){
        JSONResponse(res, 400, {"message": "Missing fields."});
    }
    console.log(req.body);
    events.create({
        name : req.body.name,
        date : req.body.date,
        organizator : req.body.org,
        description : req.body.desc
    }, function(error, event){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, event);
        }
    });
};

module.exports.eventsDelete = function(req, res){
    if(!req.params.name){
        JSONResponse(res, 400, {"message": "Missing fields."});
    }
    events.remove({name : req.params.name})
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 400, "Not found");
            return;
        }else if(error){
            JSONResponse(res, 500, "There was an error.");
            return;
        }
        JSONResponse(res, 200, "Succesfully deleted.");
    })
}

