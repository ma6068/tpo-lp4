var mongoose = require('mongoose');
require('../models/timetable');
var timetable = mongoose.model('timetable');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
    
}

module.exports.entryGetAll = function(req, res){
    timetable.find({})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, {"status" : "There was an errror"});
        }else{
            JSONResponse(res, 200, content);
        }
    });
};


module.exports.entryGetAllByEmail = function(req, res){
    if(!req.body.email){
        JSONResponse(res, 400, {
            message: "Missing entry."
        });
        return;
    }
    
    timetable.find({email : req.body.email}).
    exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};


module.exports.entryAdd = function(req, res){
    
    var entry = {
        email : req.body.email,
        name:req.body.name,
        day: req.body.day,
        start: req.body.start,
        duration : req.body.duration,
        colour : req.body.colour 
    };
    timetable.create(entry, function(error){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, entry);
        }
    });
};

module.exports.entryUpdate = function(req, res){
    if(req.params.id){
        timetable.findById(req.params.id)
        .exec(function(napaka, entry){
            if(!entry){
                JSONResponse(res, 404, "Not found.");
                return;
            }else{
                if(req.body.email){
                    entry.email = req.body.email;
                }
                if(req.body.name){
                    entry.name = req.body.name;
                }
                if(req.body.duration){
                    entry.duration = req.body.duration;
                }
                if(req.body.colour){
                    entry.color = req.body.colour;
                }
                if(req.body.day){
                    entry.day = req.body.day;
                }
                if(req.body.start){
                    entry.start = req.body.start;
                }
                entry.save(function(error, entry){
                    if(error){
                        JSONResponse(res, 400, error);
                    }else{
                        JSONResponse(res, 200, entry);
                    }
                })
            }
        })
    }
}

module.exports.entryDelete = function(req, res){
    timetable.findByIdAndRemove(req.params.id)
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 404, "Not found");
            return;
        }else if(error){
            JSONResponse(res, 500, "There was an error.");
            return;
        }
        entry.delete = true;
        JSONResponse(res, 200, entry);
    })
}

module.exports.entryDeleteByEmail = function(req, res){
    timetable.remove({email : req.params.email})
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 400, "Not found");
            return;
        }else if(error){
            JSONResponse(res, 500, "There was an error.");
            return;
        }
        JSONResponse(res, 200, "Succesfully deleted.");
    })
}
