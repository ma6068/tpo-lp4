var mongoose = require('mongoose');
require('../models/restaurant');
var restaurant = mongoose.model('Restaurant');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
};

module.exports.entryGetAll = function(req, res){
    restaurant.find({}).
    exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};

module.exports.entryAdd = function(req, res){
    if(!req.body.name || !req.body.lat || !req.body.long){
        JSONResponse(res, 400, {
            "message": "Not enough information."
        });
    }else{
        var newRes = new restaurant();
        newRes.name = req.body.name;
        newRes.lat = req.body.lat;
        newRes.long = req.body.long;
        
        if(req.body.info){
            newRes.info = req.body.info;
        }else{
            newRes.info = "";
        }
        
        newRes.save(function(error, content){
            if(error){
                JSONResponse(res, 400, error);
            }else{
                JSONResponse(res, 200, content);
            }
        });
    }
};

module.exports.entryUpdate = function(req, res){
    if(req.params.id){
        restaurant.findById(req.params.id)
        .exec(function(err, entry){
            if(err && !entry){
                JSONResponse(res, 404, "Not found.");
                return;
            }else{
                if(req.body.name){
                    entry.name = req.body.name;
                }
                if(req.body.lat){
                    entry.lat = req.body.lat;
                }
                if(req.body.long){
                    entry.long = req.body.long;
                }
                if(req.body.info){
                    entry.info = req.body.info;
                }
                entry.save(function(error, entry){
                    if(error){
                        JSONResponse(res, 400, error);
                    }else{
                        JSONResponse(res, 200, entry);
                    }
                });
            }
        });
    }
};

module.exports.entryDelete = function(req, res){
    if(req.params.id){
        restaurant.findByIdAndRemove(req.params.id)
        .exec(function(error, entry){
            if(!entry){
                JSONResponse(res, 400, "Not found");
                return;
            }else if(error){
                JSONResponse(res, 500, "There was an error.");
                return;
            }
            JSONResponse(res, 200, "Succesfully deleted.");
        });
    }
};
