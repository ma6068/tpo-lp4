var mongoose = require('mongoose');
require('../models/adminNotifications');
var adminNotifications = mongoose.model('adminNotifications');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
}

module.exports.adminNotificationsAdd = function(req, res){
    if(!req.body.notification || !req.body.date){
        JSONResponse(res, 400, {"message": "Missing fields."});
    }
    adminNotifications.create({notification : req.body.notification, date:req.body.date}, function(error, notification){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, notification);
        }
    });
    
}

module.exports.adminNotificationsClear = function(req, res) {
    adminNotifications.remove({}, function(err) {
            if (err) {
                JSONResponse(res, 400, err)
            } else {
                JSONResponse(res, 200, 'success');
            }
        }
    );
};


module.exports.adminNotificationsGetAll = function(req, res){
    adminNotifications.find({})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, {"status" : "There was en error."});
        }else{
            JSONResponse(res, 200, content);
        }
    })
}