var mongoose = require('mongoose');
require('../models/calendar');
var calendar = mongoose.model('calendar');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
};

module.exports.calendarGetAll = function(req, res){
    if(!req.body.email){
        JSONResponse(res, 400, {
            message: "Missing entry."
        });
        return;
    }
    
    calendar.find({email : req.body.email}).
    exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};

module.exports.calendarPost = function(req, res){
    if(!req.body.name || !req.body.start || !req.body.end || !req.body.email){
        JSONResponse(res, 400, {"message": "Missing fields."});
        return;
    }
    calendar.create({
        name : req.body.name,
        start : req.body.start,
        end : req.body.end,
        email: req.body.email
    }, function(error, event){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, event);
        }
    });
};
