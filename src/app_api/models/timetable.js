var mongoose = require('mongoose');

var timetableSchema = new mongoose.Schema({
    email : String,
    name : String,
    day: Number,
    start: Number,
    duration : Number,
    colour : String
});



mongoose.model('timetable', timetableSchema);