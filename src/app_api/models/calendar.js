var mongoose = require('mongoose');

var calendarSchema = new mongoose.Schema({
    email: {type: String, required: true},
    name : {type: String, required: true},
    start : {type: Date, required: true},
    end : {type: Date, required: true}
});

mongoose.model('calendar', calendarSchema, 'calendar');