var mongoose = require('mongoose');

var restaurantSchema = new mongoose.Schema({
    name : {type: String, required: true},
    lat : {type: Number, required: true},
    long : {type: Number, required: true},
    info : String
});

mongoose.model('Restaurant', restaurantSchema, 'Restaurant');