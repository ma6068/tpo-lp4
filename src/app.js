require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let session = require('express-session');
var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
  'index.controler.js': fs.readFileSync('app_client/index/index.controller.js', 'utf-8'),
  'navigation.directive.js': fs.readFileSync('app_client/shared/directive/navigation/navigation.directive.js', 'utf-8'),
  'navigation.controller.js': fs.readFileSync('app_client/shared/directive/navigation/navigation.controller.js', 'utf-8'),
  'login.controller.js': fs.readFileSync('app_client/prijava/login.controller.js', 'utf-8'),
  'register.controller.js': fs.readFileSync('app_client/registracija/register.controller.js', 'utf-8'),
  'food.controller.js': fs.readFileSync('app_client/restavracij/food.controller.js', 'utf-8'),
  'authentication.services.js': fs.readFileSync('app_client/shared/services/authentication.services.js', 'utf-8'),
  'straightAsData.service.js': fs.readFileSync('app_client/shared/services/straightAsData.service.js', 'utf-8'),
  'geolocation.service.js': fs.readFileSync('app_client/shared/services/geolocation.service.js', 'utf-8'),
  'formatDistance.filter.js': fs.readFileSync('app_client/shared/filters/formatDistance.filter.js', 'utf-8'),
  'dogodki.controller.js' : fs.readFileSync('app_client/event_manager/dogodki.controller.js', 'utf-8'),
  'token.controller.js' : fs.readFileSync('app_client/tokenConfirmation/token.controller.js', 'utf-8'),
  'resetPwd.controller.js' : fs.readFileSync('app_client/resetPwd/resetPwd.controller.js', 'utf-8'),
  'eventModal.controller.js' : fs.readFileSync('app_client/eventModal/eventModal.controller.js', 'utf-8'),
  'delete.controller.js' : fs.readFileSync('app_client/deleteModal/delete.controller.js', 'utf-8'),
  'todoModal.controller.js' : fs.readFileSync('app_client/todoModal/todoModal.controller.js', 'utf-8'),
  'editModal.controller.js' : fs.readFileSync('app_client/editModal/editModal.controller.js', 'utf-8'),
  'adminObvestila.controller.js' : fs.readFileSync('app_client/adminObvestila/adminObvestila.controller.js', 'utf-8'),
  'adminModal.controller.js' : fs.readFileSync('app_client/adminModal/adminModal.controller.js', 'utf-8'),
  'TTModal.controller.js' : fs.readFileSync('app_client/TTModal/TTModal.controller.js', 'utf-8'),
  'calendarModal.controller.js' : fs.readFileSync('app_client/calendarModal/calendarModal.controller.js', 'utf-8'),
  'bus.controller.js' : fs.readFileSync('app_client/buses/bus.controller.js', 'utf-8')
});

fs.writeFile('public/angular/straightas.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Joind script is generated "straightas.min.js".');
});

var indexRouter = require('./app_server/routes/index');
var indexApi = require('./app_api/routes/index');
var usersRouter = require('./app_server/routes/users');
var passport = require('passport');

require('./app_api/models/db');
require('./app_api/configuration/passport');
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));
app.use(express.static(path.join(__dirname, 'app_client')));
app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/api', indexApi);
app.use('/users', usersRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "message": err.name + ": " + err.message
    });
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
});


module.exports = app;
