(function() {
function registerController($window, authentication) {
  var vm = this;
  vm.regData = {};
  
  vm.sendRegisterData = function(){
    if(!vm.regData.uname || !vm.regData.pword){
      vm.error = "Please fill all the fields.";
      return false;  
    }
    if(!/\d/.test(vm.regData.pword) || !/[A-Z]/.test(vm.regData.pword) || !/[a-z]/.test(vm.regData.pword) || !vm.regData.pword.length > 7){
      vm.error = "Password needs to contain one uppercase, one lowercase letter, one number and the password needs to be longer than 7 characters.";
      return false;  
    }
    if(vm.regData.cpword != vm.regData.pword){
      vm.error = "Passwords don't match";
      return false;  
    }
    
    vm.regUser();
  };
  
  vm.regUser = function(){
      authentication.registration({
        uname: vm.regData.uname,
        pword: vm.regData.pword
      }).then(
        function success(res) {
          vm.msg = res.data.message;
        },
        function error(res) {
          vm.error = res.data.message;
        }  
      );
    };
  
}

registerController.$inject = ['$window',  'authentication'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('registerController', registerController);
})();