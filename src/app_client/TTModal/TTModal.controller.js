(function() {
  function TTModal($uibModalInstance, straightAsData, emailInfo) {
    var vm = this;
    vm.data={};
    vm.userEmail = "";
    vm.data.entry = {};
    
    if(emailInfo.email != undefined && emailInfo.email.length > 0){
      vm.userEmail = emailInfo.email;
      vm.data.entry = emailInfo.entry;
      vm.data.entry.start = emailInfo.start;
    }
    
    vm.data.name = vm.data.entry.name;
    vm.data.duration = vm.data.entry.duration;
    vm.data.colour = vm.data.entry.colour;
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.sendData = function(){
      console.log(vm.data.colour);
      if(vm.data.entry._id == null){
        straightAsData.postTT({
          name: vm.data.name,
          duration: vm.data.duration,
          day: vm.data.entry.day,
          colour: vm.data.colour,
          start: vm.data.entry.start,
          email: vm.userEmail
        }).then(
          function success(res) {
            vm.modalWin.closeS(res.data);
          },
          function error(res) {
            console.log(res);
            vm.error = res.data.message;
          }
        );
      }else{
        straightAsData.putTT(
          vm.data.entry._id,
          {
            name: vm.data.name,
            duration: vm.data.duration,
            colour: vm.data.colour,
            day: vm.data.entry.day,
            start: vm.data.entry.start
          } 
        ).then(
          function success(odgovor) {
            vm.modalWin.closeS(odgovor.data);
          },
          function error(res) {
            console.log(res);
            vm.error = res.data.message;
          }
        );
      }
      
    };
    
    vm.deleteData = function(){
      console.log(vm.data.entry);
      straightAsData.deleteTT(vm.data.entry._id)
      .then(
        function success(odgovor) {
          odgovor.data.delete = true;
          vm.modalWin.closeS(odgovor.data);
        },
        function error(res) {
          vm.error = res.data.message;
        }
      );
    };
  }
  
  TTModal.$inject = ['$uibModalInstance', 'straightAsData', 'emailInfo'];

  /* global angular */
  angular
    .module('straightas')
    .controller('TTModal', TTModal);
})();