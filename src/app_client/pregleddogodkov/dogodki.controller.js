(function(){
    function dogodkiController($scope, $window, straightAsData){
        var vm = this;
        vm.header = {
            title : 'Upcoming events'
        };
        vm.data = {};
        
    }
    
    dogodkiController.$inject = ['$scope', '$window', 'straightAsData'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('dogodkiController', dogodkiController);
})();