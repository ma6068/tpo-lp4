(function(){
    function notificationsController($scope, $window, straightAsData, $uibModal, authentication){
        var vm = this;
        vm.header = {
            title : 'Upcoming events'
        };
        vm.data = {};
        
        vm.isLogged = authentication.loggedIn();
    
        vm.currentUserStatus = authentication.currentUser().status;
        
        
        vm.isCurrentUserAdmin = vm.currentUserStatus == 2;
        
        vm.isCurrentUserEventManager = vm.currentUserStatus == 3;
        
          
        
        vm.getNotificationData = function(){
            straightAsData.notificationList().then(
                function success(response){
                    vm.data = {
                        notifications : response.data,
                    };
                    return vm.data;
                }, function error(response){
                    vm.message = "There seems to be an error";
                    console.log(response.e);
                });
        };
        
        
        vm.showNotificationModal = function() {
          var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '../adminModal/adminModal.pogled.html',
            controller: 'adminModal',
            controllerAs: 'vm'
          });
          
          
            
          primerekModalnegaOkna.result.then(function(podatki) {
            if (typeof podatki != 'undefined'){
                vm.data.notifications.push(podatki);
              }
            }, function(napaka) {
              
          });
        };
        
        
        vm.clear = function(){
            straightAsData.notificationClear().then(
                function success(response){
                    vm.data.notifications = [];
                })
                return vm.data;
        }, function error(response){
            vm.message = "There was an error.";
        }
        
      

        
       
    
        
         vm.data = vm.getNotificationData();
    }
    
    notificationsController.$inject = ['$scope', '$window', 'straightAsData', '$uibModal', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('notificationsController', notificationsController);
})();