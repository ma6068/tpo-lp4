
(function() {

function settings($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
        templateUrl: 'index.view.html',
        controller: 'indexController',
        controllerAs: 'vm'
    }).when('/login', {
        templateUrl: 'prijava/login.view.html',
        controller: 'loginController',
        controllerAs: 'vm'
    }).when('/register', {
        templateUrl: 'registracija/register.view.html',
        controller: 'registerController',
        controllerAs: 'vm'
    }).when('/food', {
        templateUrl: 'restavracij/food.view.html',
        controller: 'foodController',
        controllerAs: 'vm'
    }).when('/event-manager', { 
      templateUrl : 'event_manager/dogodki.html',
      controller : 'eventManager',
      controllerAs : 'vm'
    }).when('/events', { 
      templateUrl : 'pregleddogodkov/dogodki.html',
      controller : 'eventManager',
      controllerAs : 'vm'
    })
    .when('/confirmation/:token', { 
      templateUrl : 'tokenConfirmation/token.view.html',
      controller : 'tokenController',
      controllerAs : 'vm'
    })
    .when('/reset', { 
      templateUrl : 'resetPwd/resetPwd.view.html',
      controller : 'resetPwd',
      controllerAs : 'vm'
    })
    .when('/admin-notifications', {
      templateUrl : 'adminObvestila/adminObvestila.pogled.html',
      controller:'notificationsController',
      controllerAs:'vm'
    })
    .when('/buses', {
      templateUrl : 'buses/bus.pogled.html',
      controller : 'busController',
      controllerAs : 'vm'
    })
    .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(true);
}

/* global angular */
  angular 
    .module('straightas', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'ui.calendar']) 
    .config(['$routeProvider', '$locationProvider', settings]);
})();