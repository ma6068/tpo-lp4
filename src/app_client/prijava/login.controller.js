(function() {
function loginController($window, authentication) {
  var vm = this;
  vm.loginData = {};
  
  vm.sendLoginData = function(){
      if(!vm.loginData.uname || !vm.loginData.pword){
        vm.error = "Please fill all of the required fields.";
        return false;
      }else{
        vm.loginUser(vm.loginData.uname, vm.loginData.pword);
      }
    };
    
    vm.loginUser = function(uname, pword){
      authentication.login({
        uname: uname,
        pword: pword
      }).then(
        function success(res) {
          $window.location.href = '/';
        },
        function error(error) {
          vm.error = error.data.message;
        }  
      );
    };
}

loginController.$inject = ['$window',  'authentication'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('loginController', loginController);
})();