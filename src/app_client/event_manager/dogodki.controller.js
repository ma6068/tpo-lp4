(function(){
    function dogodkiController($scope, $window, straightAsData, $uibModal, authentication){
        var vm = this;
        vm.header = {
            title : 'Upcoming events'
        };
        vm.data = {};
        
        vm.isCurrentUserEventManager = authentication.currentUser().status == 3;
        
        vm.getEventsData = function(){
            straightAsData.eventList().then(
                function success(response){
                    vm.data = {
                        events : response.data
                    };
                    
                    for(var i in vm.data.events){
                        var as = new Date(vm.data.events[i].date);
                        vm.data.events[i].date = as.getDate() + "/" + (as.getMonth()+1) + "/" + as.getFullYear();
                    }
                    
                    return vm.data;
                }, function error(response){
                    vm.message = "There seems to be an error";
                    console.log(response.e);
                });
        };
        
        vm.showEventWindow = function() {
          var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '../eventModal/eventModal.view.html',
            controller: 'eventModal',
            controllerAs: 'vm'
          });
          
          
          primerekModalnegaOkna.result.then(function(podatki) {
            if (typeof podatki != 'undefined'){
               var as = new Date(podatki.date);
                podatki.date = as.getDate() + "/" + (as.getMonth()+1) + "/" + as.getFullYear();
                vm.data.events.push(podatki);
              }
            }, function(napaka) {
              
          });
        };
        
         vm.data = vm.getEventsData();
    }
    
    dogodkiController.$inject = ['$scope', '$window', 'straightAsData', '$uibModal', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('eventManager', dogodkiController);
})();