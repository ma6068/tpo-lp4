(function() {
  function calendarModal($uibModalInstance, straightAsData, userInfo, $filter) {
    var vm = this;
    vm.data={};
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.sendData = function(){
      straightAsData.postCalendar({
        name: vm.data.name,
        start: vm.data.start,
        end: vm.data.end,
        email: userInfo.uname
      }).then(
        function success(odgovor) {
          vm.modalWin.closeS(odgovor.data);
        },
        function error(odgovor) {
          vm.formError = "Error!";
        }
      );
    };
  }
  
  calendarModal.$inject = ['$uibModalInstance', 'straightAsData', 'userInfo', '$filter'];

  /* global angular */
  angular
    .module('straightas')
    .controller('calendarModal', calendarModal);
})();