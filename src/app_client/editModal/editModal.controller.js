(function() {
  function editModal($uibModalInstance, straightAsData, emailInfo) {
    var vm = this;
    vm.data = emailInfo;
    vm.past = {};
    vm.past.title= emailInfo.title;
    vm.past.desc = emailInfo.desc;
    
    
    vm.modalWin = {
      close: function() {
        emailInfo.title = vm.past.title;
        emailInfo.desc = vm.past.desc;
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    
    vm.editData = function(){
      console.log("click");
      straightAsData.editTodo({
          title: vm.data.title,
          desc: vm.data.desc
        }, 
        vm.data._id
      ).then(
        function success(odgovor) {
          vm.modalWin.closeS(odgovor.data);
        },
        function error(res) {
          console.log(res);
          vm.error = res.data.message;
        }
      );
    };
    
  }
  
  editModal.$inject = ['$uibModalInstance', 'straightAsData', 'emailInfo'];

  /* global angular */
  angular
    .module('straightas')
    .controller('editModal', editModal);
})();