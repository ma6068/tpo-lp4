(function() {
  function adminModal($uibModalInstance, straightAsData) {
    var vm = this;
    vm.data={};
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    

    
    
    vm.sendData = function(){
          var currentdate = new Date(); 
          var datetime =  currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
      
      console.log(datetime);
      straightAsData.notificationPost({
        notification : vm.data.notification,
        date : datetime,
      }).then(
        function success(odgovor) {
          vm.modalWin.closeS(odgovor.data);
        },
        function error(odgovor) {
          vm.formError = "Error!";
        }
      );
    };
  }
  
  adminModal.$inject = ['$uibModalInstance', 'straightAsData'];

  /* global angular */
  angular
    .module('straightas')
    .controller('adminModal', adminModal);
})();