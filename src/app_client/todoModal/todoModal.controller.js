(function() {
  function todoModal($uibModalInstance, straightAsData, emailInfo) {
    var vm = this;
    vm.data={};
    vm.userEmail = "";
    
    if(emailInfo.email != undefined && emailInfo.email.length > 0)
      vm.userEmail = emailInfo.email;
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.sendData = function(){
      straightAsData.todoPost({
        email: vm.userEmail,
        title: vm.data.title,
        desc: vm.data.desc
      }).then(
        function success(odgovor) {
          vm.modalWin.closeS(odgovor.data);
        },
        function error(res) {
          console.log(res);
          vm.error = res.data.message;
        }
      );
    };
  }
  
  todoModal.$inject = ['$uibModalInstance', 'straightAsData', 'emailInfo'];

  /* global angular */
  angular
    .module('straightas')
    .controller('todoModal', todoModal);
})();